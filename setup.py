#!/usr/bin/env python

from distutils.core import setup

from setuptools import find_packages


with open('requirements.txt', 'r') as fp:
    requirements = [line.strip() for line in fp.readlines()]


setup(
    name='oz_gdrive',
    version='1.2.0',
    description='Python Distribution Utilities',
    author='Oleksandr Zelentsov',
    author_email='saze1997@gmail.com',
    url='https://www.python.org/sigs/distutils-sig/',
    packages=find_packages(),
    install_requires=requirements,
    entry_points={
        'console_scripts': [
            'oz-gdrive-upload=oz_gdrive.main:main',
            'oz-gdrive-test=oz_gdrive.quickstart:main',
        ],
    },
    package_data={
        'oz_gdrive': [
            'logging_config.ini'
        ],
    },
)
