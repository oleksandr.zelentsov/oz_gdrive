import logging
import os
from typing import Pattern, List

from googleapiclient.discovery import build
from googleapiclient.http import MediaFileUpload
from httplib2 import Http
from oauth2client import file, client, tools

from oz_gdrive.lib import dict_cache, path_transform

JSON_CACHE_FILES_IDS = '~/.cache/oz_gdrive_files_exist.json'
FOLDER_MIME_TYPE = 'application/vnd.google-apps.folder'
SCOPES = 'https://www.googleapis.com/auth/drive'

internal_logger = logging.getLogger('oz_gdrive')


def get_credentials(token_path='token.json', credentials_path='credentials.json'):
    store = file.Storage(token_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(credentials_path, SCOPES)
        credentials = tools.run_flow(flow, store)

    return credentials


def get_service(token_path='token.json', credentials_path='credentials.json'):
    credentials = get_credentials(token_path, credentials_path)
    service = build('drive', 'v3', http=credentials.authorize(Http()))
    return service


def get_all_files(drive_service):
    results = drive_service.files().list(
        fields="files(id, name, parents)"
    ).execute()
    result = results.get('files')
    assert result is not None
    return {f['id']: f for f in result}


def get_all_folders(drive_service):
    results = drive_service.files().list(
        fields="files(id, name, parents)",
        q='mimeType={}'.format(FOLDER_MIME_TYPE),
    ).execute()
    result = results.get('files')
    assert result is not None
    return {f['id']: f for f in result}


@dict_cache(cached_argument_name='all_folders', cache_path=path_transform('~/.cache/oz_gdrive_folders.json'))
def get_full_path(*, drive, one_file, all_folders):  # TODO unused
    if isinstance(all_folders, list):
        all_folders = {f['id']: f for f in all_folders}

    if one_file.get('parents'):
        parent_folder_id = one_file['parents'][0]
        parent = all_folders.get(parent_folder_id)
        if parent is None:
            all_folders[parent_folder_id] = parent = drive.files().get(fileId=parent_folder_id).execute()

        return os.path.join(
            get_full_path(drive=drive, one_file=parent, all_folders=all_folders),
            one_file['name']
        )
    else:
        if one_file['name'].lower().strip() == 'my drive':
            return ''

        return one_file['name']


def create_folder(drive_service, path, *, folder_exist_cache):
    leading_up_to_that, last_path = os.path.split(path)
    if leading_up_to_that:
        leading_up_to_that = [create_folder(drive_service, leading_up_to_that, folder_exist_cache=folder_exist_cache)]
    else:
        leading_up_to_that = []

    cached = folder_exist_cache.get(path)
    if cached:
        internal_logger.debug('according to cache, directory you are trying to create'
                              ' (%s) already exists with id %s', path, cached)
        return cached
    else:
        exists = file_exists(drive_service, path)
        if exists:
            folder_exist_cache[path] = exists
            internal_logger.debug('directory you are trying to create (%s) already exists with id %s', path, exists)
            return exists

    internal_logger.info('creating a folder %s', path)
    file_metadata = {
        'name': last_path,
        'mimeType': FOLDER_MIME_TYPE,
        'parents': leading_up_to_that,
    }
    uploaded_folder = drive_service.files().create(
        body=file_metadata,
        fields='id'
    ).execute()
    assert uploaded_folder.get('id') is not None
    folder_exist_cache[path] = uploaded_folder.get('id')
    return uploaded_folder.get('id')


def upload_file(drive_service, local_file_path: str, remote_file_path: str, *, file_exist_cache, include_regex=None,
                exclude_regex=None):
    folder, filename = os.path.split(remote_file_path)
    if folder:
        fid = create_folder(drive_service, folder, folder_exist_cache=file_exist_cache)
    else:
        fid = 'root'

    cached = file_exist_cache.get(remote_file_path)
    if cached:
        internal_logger.debug('according to cache, file you are trying to create'
                              ' (%s) already exists with id %s', local_file_path, cached)
        return cached
    else:
        exists = file_exists(drive_service, remote_file_path)
        if exists:
            file_exist_cache[remote_file_path] = exists
            internal_logger.debug('file you are trying to upload (%s) already exists with id %s'
                                  ' and remote path %s', local_file_path, exists, remote_file_path)
            return exists

    internal_logger.info('actually uploading %s to %s', local_file_path, remote_file_path)
    file_metadata = {
        'name': filename,
        'parents': [fid],
    }
    media = MediaFileUpload(
        local_file_path,
    )
    create_request = drive_service.files().create(
        body=file_metadata,
        media_body=media,
        fields='id',
    )
    uploaded_file = create_request.execute()
    assert uploaded_file.get('id') is not None
    file_exist_cache[remote_file_path] = uploaded_file.get('id')
    return uploaded_file.get('id')


def file_exists(drive_service, remote_path):
    prev, filename = os.path.split(remote_path)
    if prev:
        prev = file_exists(drive_service, prev)
    else:  # we reached the root of disk
        prev = 'root'

    if not prev:
        return None

    found_file = drive_service.files().list(
        q='"{}" in parents and name="{}"'.format(prev, filename),
        fields='files(id)'
    ).execute()
    if found_file.get('files'):
        return found_file['files'][0]['id']
    else:
        return None


@dict_cache(JSON_CACHE_FILES_IDS, 'files_exist_cache', default_factory=dict)
def upload_path(remote_subdirectory,
                path,
                service,
                path_to_strip_from_remote_path='',
                include_regex: List[Pattern] = None,
                exclude_regex: List[Pattern] = None,
                *,
                files_exist_cache: dict):
    """
    If ``path`` is a directory,
        walk through all subdirectories and call this function on them, then
        try to upload every file in this directory using its full path.
    If ``path`` is a file,
        try to upload this file using its full path

    path_to_strip_from_remote_path serves as an initial part of path we want to cut out of our local path
    to include certain subdirectories into a disk's structure. Without it, all
    files would be uploaded to remote_subdirectory.
    """
    if exclude_regex is None:
        exclude_regex = []

    if include_regex is None:
        include_regex = []

    if os.path.isfile(path):
        remote_path = get_full_remote_path(remote_subdirectory, path_to_strip_from_remote_path, path)
        if include_regex and not any([regex.match(path) for regex in include_regex]):
            internal_logger.debug('%s is not included by regex', path)
            return []
        if exclude_regex and any([regex.match(path) for regex in exclude_regex]):
            internal_logger.debug('%s is excluded by regex', path)
            return []

        file_id = upload_file(
            service,
            local_file_path=path,
            remote_file_path=remote_path,
            include_regex=include_regex,
            exclude_regex=exclude_regex,
            file_exist_cache=files_exist_cache,
        )
        if file_id is not None:
            return [file_id]
        else:
            return []
    elif os.path.isdir(path):
        internal_logger.debug('walking through %s in search of files', path)
        results = []
        for top_dir, dir_names, file_names in os.walk(path):
            for path_ in file_names:
                full_file_name = os.path.join(top_dir, path_)
                if include_regex and not any([regex.match(full_file_name) for regex in include_regex]):
                    internal_logger.debug('%s is not included by regex', full_file_name)
                    continue
                if exclude_regex and any([regex.match(full_file_name) for regex in exclude_regex]):
                    internal_logger.debug('%s is excluded by regex', full_file_name)
                    continue

                _, filename = os.path.split(full_file_name)
                file_id = upload_file(
                    service,
                    full_file_name,
                    get_full_remote_path(
                        remote_subdirectory,
                        local_origin=path_to_strip_from_remote_path,
                        path=full_file_name,
                    ),
                    file_exist_cache=files_exist_cache,
                    include_regex=include_regex,
                    exclude_regex=exclude_regex,
                )
                if file_id is not None:
                    results += [file_id]

            for one_dir in dir_names:
                full_file_name = os.path.join(top_dir, one_dir)
                result = upload_path(
                    remote_subdirectory,
                    full_file_name,
                    service,
                    path_to_strip_from_remote_path,
                    files_exist_cache=files_exist_cache,
                    include_regex=include_regex,
                    exclude_regex=exclude_regex
                )
                if isinstance(result, list):
                    results += result
                elif result is not None:
                    results += [result]
            break

        return results

    else:
        internal_logger.error('failed to upload %s, neither a file nor a directory', path)
        return None


def get_full_remote_path(remote_subdirectory, local_origin, path):
    """
    Build a full remote path this way:
    - remote_subdirectory is a base of all work on our behalf on the remote disk
    - local_origin is a part of path which we wanna cut
    - path is a full local path to a file we wanna upload
    """
    if len(local_origin) != 0:
        return os.path.join(
            remote_subdirectory,
            path[len(local_origin) + 1:]
        )

    return os.path.join(
        remote_subdirectory,
        path,
    )

