import json
import logging
from copy import copy
from functools import reduce
from json import JSONDecodeError
from os.path import isfile, abspath, expanduser


def function_composition(*functions):
    if len(functions) == 0:
        raise ValueError('cannot compose 0 functions')
    elif len(functions) == 1:
        return functions[0]
    elif len(functions) == 2:
        return lambda arg: functions[0](functions[1](arg))
    else:
        return reduce(function_composition, functions)


path_transform = function_composition(abspath, expanduser)
default_cache_path = path_transform('~/.cache/oz_gdrive_cache')
internal_logger = logging.getLogger('oz_gdrive')


def get_default(default, default_factory):
    if default is not None:
        return default
    else:
        return default_factory()


def dict_cache(cache_path=default_cache_path,
               cached_argument_name='cached',
               default=None,
               default_factory=lambda: None):  # todo handle no cache
    cache_path = path_transform(cache_path)

    def decorator(f):
        def func_wrapper(*args, **kwargs):
            if cached_argument_name not in kwargs:
                if not isfile(cache_path):
                    val = get_default(default, default_factory)
                    internal_logger.debug('no such file: %s, setting %s', cache_path, val)
                else:
                    try:
                        with open(cache_path, 'r') as fp:
                            val = json.load(fp)
                    except (IOError, JSONDecodeError) as e:
                        internal_logger.exception(e)
                        val = get_default(default, default_factory)

                kwargs.update({cached_argument_name: val})
            else:
                val = kwargs[cached_argument_name]

            prev_val = copy(val)
            try:
                return f(*args, **kwargs)
            finally:
                if prev_val != val:
                    with open(cache_path, 'w') as fp:
                        json.dump(val, fp)

        return func_wrapper
    return decorator
