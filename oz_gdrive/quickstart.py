from oz_gdrive.google_drive_filesystem_tools import get_full_path, get_all_files, get_service


def main():
    """Shows basic usage of the Drive v3 API.
    Prints the names and ids of the first 10 files the user has access to.
    """
    service = get_service()

    # Call the Drive v3 API
    results = service.files().list(
        pageSize=10,
        fields="nextPageToken, files(id, name)"
    ).execute()
    items = results.get('files', [])

    if not items:
        print('No files found.')
    else:
        print('Files:')
        for item in items:
            print(u'{0} ({1})'.format(item['name'], item['id']))


if __name__ == '__main__':
    s = get_service()
    fs = get_all_files(s)
    print(fs)
