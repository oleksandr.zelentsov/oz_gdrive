#!/usr/bin/env python3
import logging
import os
import re
from argparse import ArgumentParser
from logging.config import fileConfig

from pid import PidFileError, PidFile

from oz_gdrive.google_drive_filesystem_tools import get_service, upload_path
from oz_gdrive.lib import path_transform

default_logging_config_filename = os.path.join(os.path.split(os.path.realpath(__file__))[0], 'logging_config.ini')


def get_args(args=None):
    parser = ArgumentParser(description='Upload files to Google drive.')
    parser.add_argument('files', nargs='+', type=str, help='files to upload; use directory to upload all files and'
                                                           ' directories from it to --remote-subdirectory')
    parser.add_argument('-r', '--remote-subdirectory', default='oz_gdrive_files', help='directory that will '
                                                                                       'be the "root" for all uploaded'
                                                                                       ' files')
    parser.add_argument('-t', '--token-path', default='token.json', help='path to a possibly existing token.json file'
                                                                         'for google api auth')
    parser.add_argument('-c', '--credentials-path', default='credentials.json', help='path to credentials for'
                                                                                     ' google api auth')
    parser.add_argument('--logging-config', default=default_logging_config_filename,
                        help='path to a custom python logging ini config')
    parser.add_argument('--regex-include', type=str, nargs='+', default=[], help='limit the file uploading to the '
                                                                                 'paths that match this regex')
    parser.add_argument('--regex-exclude', type=str, nargs='+', default=[], help='exclude paths from file uploading'
                                                                                 ' that match this regex')

    return parser.parse_args(args)


def main():
    args = get_args()
    internal_logger = setup_logging(args)
    try:
        with PidFile('oz_gdrive', '/tmp'):
            setup_regexes(args, internal_logger)
            service = get_service(path_transform(args.token_path), path_transform(args.credentials_path))
            results = []
            for path in map(path_transform, args.files):
                if os.path.isdir(path):
                    part_of_path_to_not_include_on_remote = path
                elif os.path.isfile(path):
                    part_of_path_to_not_include_on_remote, _ = os.path.split(path)
                else:
                    internal_logger.error('skipping %s: file or directory not found', path)
                    continue

                uploaded_ids = upload_path(
                    args.remote_subdirectory,
                    path,
                    service,
                    path_to_strip_from_remote_path=part_of_path_to_not_include_on_remote,
                    include_regex=args.regex_include,
                    exclude_regex=args.regex_exclude)
                if uploaded_ids is not None:
                    results += uploaded_ids

            internal_logger.info('uploaded %s files in total, ids:', len(results))
            for result in results:
                internal_logger.info(result)
    except PidFileError:
        internal_logger.fatal('the program is already running')
        exit(3)


def setup_regexes(args, internal_logger):
    try:
        internal_logger.debug('compiling regexes')
        args.regex_include = [re.compile(regex) for regex in args.regex_include]
        args.regex_exclude = [re.compile(regex) for regex in args.regex_exclude]
    except Exception as e:
        internal_logger.exception(e)
        internal_logger.fatal('could not compile regexes')
        exit(2)


def setup_logging(args):
    try:
        fileConfig(args.logging_config)
    except Exception as e:
        fileConfig(default_logging_config_filename)
    else:
        e = None
    internal_logger = logging.getLogger('oz_gdrive')
    if e:
        internal_logger.exception(e)
    return internal_logger


if __name__ == '__main__':
    main()
